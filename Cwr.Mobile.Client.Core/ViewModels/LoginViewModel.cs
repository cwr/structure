using Cirrious.MvvmCross.ViewModels;
using System.Windows.Input;

namespace Cwr.Mobile.Client.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        public ICommand LoginCommand
        {
            get { return new MvxCommand(() => ExecuteLogin()); }
        }
        
        private string _serverUrl;
        public string ServerUrl
        {
            get { return _serverUrl; }
            set 
            {
                _serverUrl = value;
                RaisePropertyChanged(() => ServerUrl);
            }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set 
            {
                _userName = value;
                RaisePropertyChanged(() => UserName); 
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }


        private void ExecuteLogin()
        {
            // TODO: Perform Login.
        }
    }
}
