using System.Drawing;
using Cwr.Mobile.Client.Core.ViewModels;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace Cwr.Mobile.Client.IOS.Views
{
    [Register("UniversalView")]
    public class UniversalView : UIView
    {
        public UniversalView()
        {
            Initialize();
        }

        public UniversalView(RectangleF bounds)
            : base(bounds)
        {
            Initialize();
        }

        void Initialize()
        {
            BackgroundColor = UIColor.White;
        }
    }

    [Register("LoginView")]
    public class LoginView : MvxViewController
    {
        public LoginView()
        {
        }

        public override void ViewDidLoad()
        {
            View = new UniversalView();

            base.ViewDidLoad();

            // ios7 layout
            if (RespondsToSelector(new Selector("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            // Server Url.
            var labelServerUrl = new UILabel(new RectangleF(0, 0, 320, 40));
            labelServerUrl.Text = "Server Url";
            Add(labelServerUrl);

            var textServerUrl = new UITextField(new RectangleF(0, 50, 320, 40));
            Add(textServerUrl);

            // User Name.
            var labelUserName = new UILabel(new RectangleF(0, 100, 320, 40));
            labelUserName.Text = "User";
            Add(labelUserName);

            var textUserName = new UITextField(new RectangleF(0, 150, 320, 40));
            Add(textUserName);

            // Password
            var labelPassword = new UILabel(new RectangleF(0, 200, 320, 40));
            labelPassword.Text = "Password";
            Add(labelPassword);

            var textPassword = new UITextField(new RectangleF(0, 250, 320, 40));
            textPassword.SecureTextEntry = true;
            Add(textPassword);
            
            // Login Button.
            var button = AddButton(0, "Login");
            Add(button);

            var set = this.CreateBindingSet<LoginView, LoginViewModel>();
            set.Bind(labelPassword).To(vm => vm.ServerUrl);
            set.Bind(textUserName).To(vm => vm.UserName);
            set.Bind(textPassword).To(vm => vm.Password);
            set.Bind(button).To(vm => vm.LoginCommand); 
            set.Apply();
        }
    
        private UIButton AddButton(int count, string title)
        {
            var button = new UIButton(UIButtonType.RoundedRect);
            button.Frame = new RectangleF(10, 10 + count * 40, 300, 40);
            button.SetTitle(title, UIControlState.Normal);
            
            return button;
        }
    }
}
